## データの取得とフォーマッティング。

```r
library(ggplot2)
library(magrittr)
library(tidyr)
library(dplyr)
library(lme4)
library(lmerTest)

fixation <- read.csv("./output.csv", header =T)
summary(fixation)

# Bin分割
span_begin = 2000
span_end   = 8000
fixation %<>% filter(GazeEnd>0)
seq_span   = seq(from=span_begin, to=span_end, 20)
binary_data= matrix(data=span_begin,
                    nrow=nrow(fixation),
                    ncol=length(seq_span))
seq_span   ->colnames(binary_data)

# gazeがあったかどうかを見ている。
for (i in 1:length(seq_span)){
    binary_data[,i] = ifelse(
        ((span_begin + i*20) > fixation$GazeStart &
         (span_begin + i*20) < fixation$GazeEnd),
        1,
        0)}
binary_data %>% head(100) %>% View()

# 整形
fixation %>% 
    mutate(
        Target = ifelse(AOI == 1, as.character(AOI1), "BackGround"),
        Target = ifelse(AOI == 2, as.character(AOI2), Target),
        Target = ifelse(AOI == 3, as.character(AOI3), Target),
        Target = ifelse(AOI == 4, as.character(AOI4), Target)) %>% 
    select(ParticipantName, ItemNo, Condition, AOI, Target) %>% 
    cbind(as.data.frame(binary_data)) %>%
    gather(variable, value,
           -ParticipantName,-ItemNo,-Condition,-AOI,-Target)%>%
    arrange(ParticipantName,ItemNo) %>%
    mutate(variable = as.factor(variable)) %>%
    group_by(ParticipantName,ItemNo,Condition,AOI,Target,variable)%>% 
    summarise(value = sum(value)) %>% 
    rename(subj=ParticipantName,item=ItemNo,cond=Condition,
           variable=Target,bin=variable) %>% 
    ungroup %>%
    select(-AOI) %>%
    arrange(subj,item) %>% 
    na.omit() %>%
    reshape2::dcast(subj + item + cond + bin ~ variable, sum) %>% 
    # itemは崩す。Arai2016のp211, Barr2008にある。おもしろい。
    group_by(subj,cond,bin) %>%
    summarise_all(funs(sum)) %>%
    ungroup %>%
    select(-item) %>%
    as.data.frame %>%
    mutate(Particle=as.factor(ifelse(cond=="a"|cond=="c",
                              "NPI",
                              "Non-NPI")),
           Local=as.factor(ifelse(cond=="a"|cond=="b",
                              "POS",
                              "NEG"))) %>%
    select(-cond) %>%
    select(subj, Particle, Local, bin, A,B,C,D, BackGround) %>%
    rename(Subject=subj,Bin=bin,
           N1V1=A, N1notV1=B, N2V1=C, N2notV1=D,
           Background=BackGround) ->
fixation.tidy
save(fixation.tidy, file="fixation.tidy.Rdata") 
```
